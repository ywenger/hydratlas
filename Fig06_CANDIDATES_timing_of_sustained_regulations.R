# global changes: ifelse to if_else
# https://stackoverflow.com/questions/22523131/dplyr-summarise-equivalent-of-drop-false-to-keep-groups-with-zero-length-in

setwd("/Users/Yvan/Documents/Publications_PROJECTS/hydratlas_now/")
inputdir <- "/Users/Yvan/Documents/Publications_PROJECTS/hydratlas_now/Rcode/"
date <- Sys.Date()
version <- "hydratlas_v3"
dir.create(version, showWarnings = F)

dir <- "Figure06"
fig <- "Fig"
sfig <- "SFig"
stable <- "STable"
dir.create(paste0(version, "/", dir), showWarnings = F)
outputdir <- paste0(version, "/", dir)

# Load required packages
if(!require(pacman)){
  install.packages("pacman")
  library(pacman)
}

p_load(tidyverse)
p_load(DESeq2)
# p_load(reshape2)
p_load(openxlsx)
p_load(ggrepel)
p_load(pheatmap)

theme_main <- theme(strip.text = element_text(size = 8), axis.text = element_text(size = 7.5), axis.title = element_text(size = 9))

# contains results of blastx to hydra proteins (E-val < 1e-50), human reference proteome (E-val < 1e-3)
# Panther13 annotation and Pfam 31.0 annotation
annotation_HvJussy <- read_tsv("https://ndownloader.figshare.com/files/11981084", col_names = TRUE)

times <- c(0, 0.5, 1, 2, 4, 8, 16, 24, 36, 48, 99)
# tpms <- read_tsv("Rcode/TPMs_2017_reg.csv") %>% mutate(names = substr(names, 0, 17))
countData <- read.table(paste0(inputdir, "counts2017_reg_1rpm.csv"), row.names = 1, header = T)
countData_reg <- countData %>% dplyr::select(c(1:30, 91:93, 31:60, 94:96)) %>% rownames_to_column(var = "names")

# select transcripts with at least one median replicate >= 100
# in this particular analysis selecting for at least ons sample >= 100 caused
# problems with some transcripts with only one high sample among triplicates
countData_regSlim_AR <- countData_reg %>% select(names, contains("H5"), contains("Head")) %>%
  gather(variable, value, -names) %>% arrange(names, variable) %>% mutate(condition = str_sub(variable, 6, 11)) %>%
  group_by(names, condition) %>% summarize(medvalue = median(value)) %>% filter(medvalue >= 100) %>%
  semi_join(countData_reg, ., by = "names") %>% select(names, contains("H5"), contains("Head")) %>%
  column_to_rownames(var = "names")

countData_regSlim_BR <- countData_reg %>% select(names, contains("F5"), contains("Foot")) %>%
  gather(variable, value, -names) %>% arrange(names, variable) %>% mutate(condition = str_sub(variable, 6, 11)) %>%
  group_by(names, condition) %>% summarize(medvalue = median(value)) %>% filter(medvalue >= 100) %>%
  semi_join(countData_reg, ., by = "names") %>% select(names, contains("F5"), contains("Foot")) %>%
  column_to_rownames(var = "names")
# names(countData_reg)
colData_reg <- data.frame(condition = rep(c("BR50", "AR50"), each  = 33),
                          hours = rep(c("0","0.5","1","2","4","8","16","24","36","48", "99"), each  = 3, time = 2),
                          row.names = colnames(countData_reg %>% select(-names)))

### run DESeq2
dds_AR50 <- DESeqDataSetFromMatrix(countData = countData_regSlim_AR, colData = colData_reg[34:66,], design = ~ hours)
dds_BR50 <- DESeqDataSetFromMatrix(countData = countData_regSlim_BR, colData = colData_reg[1:33,], design = ~ hours)

dds_AR50 <- DESeq(dds_AR50, parallel = TRUE, betaPrior = TRUE)
dds_BR50 <- DESeq(dds_BR50, parallel = TRUE, betaPrior = TRUE) 

# Calculate contrasts for every time and extract coded values for significance (df_AR), log2FC, FDR from DESeq2
vecnames <- c("005", "010", "020", "040", "080", "160", "240", "360", "480", "999")
vecnames_AR <- str_c("reg", vecnames, "_AR")
vecnames_BR <- str_c("reg", vecnames, "_BR")
vectimes <- c("0.5", "1", "2", "4", "8", "16", "24", "36", "48", "99")
padj_threshold = 1e-3
log2FC_threshold = 1

df_AR <- data.frame(NULL, stringsAsFactors = FALSE)
FC_AR <- data.frame(NULL, stringsAsFactors = FALSE)
padj_AR <- data.frame(NULL, stringsAsFactors = FALSE)
for(i in seq_along(vectimes)){
  # extract results from DESeq2 object
  results <- results(dds_AR50, contrast = c("hours", vectimes[i], "0"), tidy = TRUE) %>% dplyr::rename(names = row)
  # compute significance matrix
  df_AR <- results %>%
    mutate(!!vecnames_AR[i] := if_else(padj < padj_threshold & abs(log2FoldChange) > log2FC_threshold, sign(log2FoldChange), 0)) %>%
    select(names, vecnames_AR[i]) %>%
    bind_cols(df_AR)
  # compute log2foldchange matrix
  FC_AR <- results %>%
    select(names, log2FoldChange, !!vecnames_AR[i] := log2FoldChange) %>%
    bind_cols(FC_AR)
  # compute FDR
  padj_AR <- results %>%
    select(names, padj, !!vecnames_AR[i] := padj) %>%
    bind_cols(padj_AR)
  print(paste("iteration", i, vecnames_AR[i]))
}

# Remove redundant columns with names from dataset
log2fc_signif_AR <- df_AR %>% select(names, rev(starts_with("reg"))) %>%
  filter_all(.vars_predicate = all_vars(!is.na(.))) %>%
  column_to_rownames(var = "names") 
log2fc_AR <- FC_AR %>% select(names, rev(starts_with("reg")))
FDR_AR <- padj_AR %>% select(names, rev(starts_with("reg")))

df_BR <- data.frame(NULL, stringsAsFactors = FALSE)
FC_BR <- data.frame(NULL, stringsAsFactors = FALSE)
padj_BR <- data.frame(NULL, stringsAsFactors = FALSE)
for(i in seq_along(vectimes)){
  # extract results from DESeq2 object
  results <- results(dds_BR50, contrast = c("hours", vectimes[i], "0"), tidy = TRUE) %>% dplyr::rename(names = row)
  # compute significance matrix
  df_BR <- results %>%
    mutate(!!vecnames_BR[i] := if_else(padj < padj_threshold & abs(log2FoldChange) > log2FC_threshold, sign(log2FoldChange), 0)) %>%
    select(names, vecnames_BR[i]) %>%
    bind_cols(df_BR)
  # compute log2foldchange matrix
  FC_BR <- results %>%
    select(names, log2FoldChange, !!vecnames_BR[i] := log2FoldChange) %>%
    bind_cols(FC_BR)
  # 
  padj_BR <- results %>%
    select(names, padj, !!vecnames_BR[i] := padj) %>%
   bind_cols(padj_BR)
  print(paste("iteration", i, vecnames_BR[i]))
}
log2fc_signif_BR <- df_BR %>% select(names, rev(starts_with("reg"))) %>%
  filter_all(.vars_predicate = all_vars(!is.na(.))) %>%
  column_to_rownames(var = "names")
log2fc_BR <- FC_BR %>% select(names, rev(starts_with("reg")))
FDR_BR <- padj_BR %>% select(names, rev(starts_with("reg")))

# Obtain timing onset of sustained regulation
# Thanks to Leon Fodoulian (Unige) for this part
signif_AR_onset <- apply(log2fc_signif_AR, 1, function(x, conditions = colnames(log2fc_signif_AR)) {
  last.time.point <- x[length(x)]
  for (i in rev(1:length(x))) {
    if (x[i] == last.time.point) {
      j <- i
      next
    } else if (x[i] != last.time.point) {
      break
    }
  }
  if (x[j] != 0 & all(x[j:length(x)] == x[j]) & x[j] == 1) {
    paste("pos", conditions[j], sep = "_")
  } else if (x[j] != 0 & all(x[j:length(x)] == x[j]) & x[j] == -1) {
    paste("neg", conditions[j], sep = "_")
  } else {
    "not_sustained" }
}) %>% data.frame(reg_onset = ., stringsAsFactors = FALSE) %>% rownames_to_column(var = "names")

# Add annotation for supplementary info
AR_log2fc_signif_save <- left_join(log2fc_signif_AR %>% rownames_to_column(var = "names"), signif_AR_onset, by = "names") %>%
  left_join(annotation_HvJussy, by = "names") %>% arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")
AR_log2fc_save <- left_join(log2fc_AR, signif_AR_onset, by = "names") %>% left_join(annotation_HvJussy, by = "names") %>%
  arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")
AR_FDR_save <- left_join(FDR_AR, signif_AR_onset, by = "names") %>% left_join(annotation_HvJussy, by = "names") %>%
  arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")

signif_BR_onset <- apply(log2fc_signif_BR, 1, function(x, conditions = colnames(log2fc_signif_BR)) {
  last.time.point <- x[length(x)]
  for (i in rev(1:length(x))) {
    if (x[i] == last.time.point) {
      j <- i
      next
    } else if (x[i] != last.time.point) {
      break
    }
  }
  if (x[j] != 0 & all(x[j:length(x)] == x[j]) & x[j] == 1) {
    paste("pos", conditions[j], sep = "_")
  } else if (x[j] != 0 & all(x[j:length(x)] == x[j]) & x[j] == -1) {
    paste("neg", conditions[j], sep = "_")
  } else {
    "not_sustained" }
}) %>% data.frame(reg_onset = ., stringsAsFactors = FALSE) %>% rownames_to_column(var = "names")

# Add annotation for supplementary info
BR_log2fc_signif_save <- left_join(log2fc_signif_BR %>% rownames_to_column(var = "names"), signif_BR_onset, by = "names") %>%
  left_join(annotation_HvJussy, by = "names") %>% arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")
BR_log2fc_save <- left_join(log2fc_BR, signif_BR_onset, by = "names") %>% left_join(annotation_HvJussy, by = "names") %>%
  arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")
BR_FDR_save <- left_join(FDR_BR, signif_BR_onset, by = "names") %>% left_join(annotation_HvJussy, by = "names") %>%
  arrange(reg_onset, names) %>% filter(reg_onset != "not_sustained")

# Save excel workbook
wb <- createWorkbook(creator = "YW")
addWorksheet(wb = wb, sheetName = "ApicalReg_signif_FC")
addWorksheet(wb = wb, sheetName = "ApicalReg_FC")
addWorksheet(wb = wb, sheetName = "ApicalReg_FDR")
addWorksheet(wb = wb, sheetName = "BasalReg_signif_FC")
addWorksheet(wb = wb, sheetName = "BasalReg_FC")
addWorksheet(wb = wb, sheetName = "BasalReg_FDR")
writeData(wb = wb, sheet = "ApicalReg_signif_FC", x = AR_log2fc_signif_save)
writeData(wb = wb, sheet = "ApicalReg_FC", x = AR_log2fc_save)
writeData(wb = wb, sheet = "ApicalReg_FDR", x = AR_FDR_save)
writeData(wb = wb, sheet = "BasalReg_signif_FC", x = BR_log2fc_signif_save)
writeData(wb = wb, sheet = "BasalReg_FC", x = BR_log2fc_save)
writeData(wb = wb, sheet = "BasalReg_FDR", x = BR_FDR_save)
saveWorkbook(wb = wb, file = paste0(outputdir, "/STable_significance_of_modulations_by_timepoint_", date, ".xlsx"), overwrite = TRUE)


# Get transcripts with sustained regulations that are specific to AR-50/BR-50
# NAs are introduce when a gene does not reach 100 reads in any condition: "not_sustained"
# Here, a transcript with a flat expression pattern over 48 hours but which is differentially regulated
# in the Basal/Apical part of the animal (homeostatic) is considered as sustained as we can predict
# it will be modulated at some point to reach its hemeostatic level.
modul_specific <- full_join(signif_AR_onset, signif_BR_onset, by = "names", suffix = c("_AR", "_BR")) %>% as.tibble() %>%
  replace_na(list(reg_onset_AR = "not_sustained", reg_onset_BR = "not_sustained")) %>%
  # filter(reg_onset_AR != "not_sustained" | reg_onset_BR != "not_sustained") %>%
  mutate(reg_type = if_else(str_sub(reg_onset_AR, 0, 3) == str_sub(reg_onset_BR, 0, 3), "unspecific",
                    if_else(str_sub(reg_onset_AR, 0, 3) == "pos" & str_sub(reg_onset_BR, 0, 3) == "not", reg_onset_AR,
                    if_else(str_sub(reg_onset_AR, 0, 3) == "not" & str_sub(reg_onset_BR, 0, 3) == "pos", reg_onset_BR,
                    if_else(str_sub(reg_onset_AR, 0, 3) == "neg" & str_sub(reg_onset_BR, 0, 3) == "not", reg_onset_AR,
                    if_else(str_sub(reg_onset_AR, 0, 3) == "not" & str_sub(reg_onset_BR, 0, 3) == "neg", reg_onset_BR, "inverted"))))))

specific_AR <- full_join(signif_AR_onset, signif_BR_onset, by = "names", suffix = c("_AR", "_BR")) %>%
  as_tibble() %>% filter(reg_onset_AR != "not_sustained") %>%
  mutate(reg_onset_BR = if_else(str_sub(reg_onset_AR, 0, 3) == str_sub(reg_onset_BR, 0, 3), "unspecific", "AR50_specific")) %>%
  mutate(reg_onset_BR = str_replace_na(string = reg_onset_BR)) %>%
  mutate(reg_onset_BR = str_replace(string = reg_onset_BR, pattern = "NA", replacement = "AR50_specific"))

levels_AR <- c(expand.grid(c("neg_reg", "pos_reg"), c("999", "480", "360", "240", "160", "080", "040", "020", "010", "005")) %>%
                 mutate(get = paste0(Var1, Var2, "_AR")) %>% pull(get), "not_sustained")

specific_AR_summary <- specific_AR %>% mutate(reg_onset_AR = factor(reg_onset_AR, levels = levels_AR)) %>%
  select(-names) %>% table() %>% as.data.frame() %>% filter(reg_onset_AR != "not_sustained")


specific_BR <- full_join(signif_BR_onset, signif_AR_onset, by = "names", suffix = c("_BR", "_AR")) %>%
  as_tibble() %>% filter(reg_onset_BR != "not_sustained") %>%
  mutate(reg_onset_AR = if_else(str_sub(reg_onset_BR, 0, 3) == str_sub(reg_onset_AR, 0, 3), "unspecific", "BR50_specific")) %>%
  mutate(reg_onset_AR = str_replace_na(string = reg_onset_AR)) %>%
  mutate(reg_onset_AR = str_replace(string = reg_onset_AR, pattern = "NA", replacement = "BR50_specific"))

levels_BR <- c(expand.grid(c("neg_reg", "pos_reg"), c("999", "480", "360", "240", "160", "080", "040", "020", "010", "005")) %>%
                 mutate(get = paste0(Var1, Var2, "_BR")) %>% pull(get), "not_sustained")

specific_BR_summary <- specific_BR %>% mutate(reg_onset_BR = factor(reg_onset_BR, levels = levels_BR)) %>%
  select(-names) %>% table() %>% as.data.frame() %>% filter(reg_onset_BR != "not_sustained")

vectime <- c("30\'", "1", "2", "4", "8", "16", "24", "36", "48 hpa", "Intact")

reg_onset <- bind_rows("AR50" = specific_AR_summary, "BR50" = specific_BR_summary, .id = "reg_type") %>%
  arrange(reg_type, reg_onset_AR, reg_onset_BR) %>%
  mutate(time = factor(c(rep(vectime, each = 2, length.out = n()/2), rep(vectime, length.out = n()/2)), levels = vectime)) %>%
  mutate(type = if_else(reg_type == "AR50", substr(reg_onset_AR, 0, 3), substr(reg_onset_BR, 0, 3))) %>%
  mutate(Freq = if_else(type == "neg", -1*as.numeric(Freq), as.numeric(Freq))) %>% # known issue on current version: https://github.com/tidyverse/dplyr/issues/2365
  mutate(relationship = if_else(reg_type == "AR50" & reg_onset_BR == "unspecific", "unspecific",
                               if_else(reg_type == "BR50" & reg_onset_AR == "unspecific", "unspecific",
                                       if_else(reg_type == "AR50", "specific to AR-50", "specific to BR-50"))))

ggplot(data = reg_onset %>% filter(time != "Intact"), aes(x = as.factor(time), y = Freq, fill = relationship)) +
  geom_bar(aes(group = relationship), stat = "identity", position = "dodge", color = "black", size = 0.25) +
  geom_text(aes(group = relationship, label = Freq), position=position_dodge(width=0.9), vjust=-0.5, size = 2.25) +
  facet_wrap(~reg_type) +
  scale_fill_manual(values = c("specific to AR-50" = "#33CCCC", "specific to BR-50" = "#FF9933", "unspecific" = "lightgrey")) +
  xlab("Timing of onset of sustained expression") +
  ylab("Number of transcripts") +
  # scale_y_continuous(breaks = seq(-200, +400, by = 100)) +
  # expand_limits(y = c(-200, +400)) +
  theme_bw() + theme_main
ggsave(filename = paste0(outputdir, "/Fig_onset_of_regulations_", date,  ".pdf"), useDingbats = FALSE, width = 20, height = 8, units = "cm")



ggplot(data = reg_onset, aes(x = as.factor(time), y = Freq, fill = relationship, group = relationship)) +
  geom_bar(stat = "identity", position = "dodge", color = "black", size = 0.25) +
  geom_text(aes(label = Freq), position=position_dodge(width=0.9), vjust = -0.5, size = 2.25) +
#  geom_text_repel(aes(group = relationship, label = Freq), size = 2.25, size = 2.25, direction = "y") +
  facet_wrap(~reg_type) +
  scale_fill_manual(values = c("specific to AR-50" = "#33CCCC", "specific to BR-50" = "#FF9933", "unspecific" = "lightgrey")) +
  xlab("Timing of onset of sustained expression") +
  ylab("Number of transcripts") +
  # scale_y_continuous(breaks = seq(-200, +400, by = 100)) +
  # expand_limits(y = c(-200, +400)) +
  theme_bw() + theme_main
ggsave(filename = paste0(outputdir, "/SFig_onset_of_regulations_with_last_timepoint_", date,  ".pdf"), useDingbats = FALSE, width = 20, height = 8, units = "cm")



dir <- "Figure07"
dir.create(paste0(version, "/", dir), showWarnings = F)
outputdir <- paste0(version, "/", dir)

# Annotation TFs & signaling molecules
# Retrieve Panther families and sub-families that are classified as TF (PC00218)
panther_classification <- read.delim(paste0(inputdir, "PANTHER13.0_HMM_classifications_v3"), header = F, sep = "\t", na = c("","NA"), stringsAsFactors = FALSE)
colnames(panther_classification) <- c("PANTHER.annotation", "MolFun", "BiolProc", "CellComp", "ProtClass", "Pathway")
panther_TF <- panther_classification %>% filter(str_detect(ProtClass, pattern = "PC00218"))
panther_TF_families <- panther_TF$PANTHER.annotation %>% str_match("PTHR\\d+(\\:SF\\d+)*") %>% .[,1]
Jussy_PANTHER_TF <- annotation_HvJussy %>% filter(str_detect(PANTHER_annotation, pattern = str_c(panther_TF_families, collapse = "|"))) %>% pull(names)

# Retrieve the list of pfam TFs in DBD
# If not available anymore get it from https://ndownloader.figshare.com/files/12102527
pfams_TF_families <- read_tsv("http://www.transcriptionfactor.org/Download/pfam_18.dbds.v2.03.txt", col_names = F, skip = 1) %>%
  select(X2, X4) %>% dplyr::rename(pfamID = X2, description = X4)
Jussy_PFAM_TF <- annotation_HvJussy %>% filter(str_detect(PFAM_annotation, pattern = str_c(pfams_TF_families$pfamID, collapse = "|"))) %>% pull(names)
# TF dataset ()
TF_all <- dplyr::union(Jussy_PANTHER_TF, Jussy_PFAM_TF)

# Signaling molecules if predicted signal peptide by Phobius or SignalP
# Phobius (v1.01) : http://phobius.sbc.su.se/ 
# SignalP (v4.1):   http://www.cbs.dtu.dk/services/SignalP/
# using strict or nonstrict in-silico translation mode.
# Using transcriptome deposited on GEO: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE111531
SP_all <- read_tsv("Rcode/summary_signaling_molecules_19062018.csv", col_names = FALSE) %>% pull(X1)


# Plot individual candidates
selection_reg_onset <- c("reg005", "reg010", "reg020", "reg040", "reg080")
# selection_reg_onset <- c("reg005", "reg010", "reg020", "reg040")

# Get candidates, that are modulated between 30' and 8hpa, and that are sustained and specific
candidates_AR <- modul_specific %>% filter(str_detect(reg_onset_AR, pattern = str_c(selection_reg_onset, "_AR", collapse = "|")) & reg_type != "unspecific")
candidates_BR <- modul_specific %>% filter(str_detect(reg_onset_BR, pattern = str_c(selection_reg_onset, "_BR", collapse = "|")) & reg_type != "unspecific")

candidates_AR_FC <- semi_join(x = log2fc_AR, y = candidates_AR, by = "names")
candidates_BR_FC <- semi_join(x = log2fc_BR, y = candidates_BR, by = "names")

candidates_AR_FC_TF <- candidates_AR_FC %>% gather(variable, value, -names) %>% 
  filter(str_detect(names, str_c(TF_all, collapse = "|"))) %>%
  mutate(type = "Transcription factors")

candidates_AR_FC_SP <- candidates_AR_FC %>% gather(variable, value, -names) %>%
  filter(str_detect(names, str_c(SP_all, collapse = "|"))) %>%
  mutate(type = "Signaling molecules")

candidates_AR_FC_others <- candidates_AR_FC %>% gather(variable, value, -names) %>%
  filter(!str_detect(names, str_c(SP_all, collapse = "|"))) %>%
  filter(!str_detect(names, str_c(TF_all, collapse = "|"))) %>%
  mutate(type = "Others")

candidates_AR_FC_annotated <- rbind(candidates_AR_FC_TF, candidates_AR_FC_SP, candidates_AR_FC_others) %>%
  arrange(names, variable) %>% mutate(time = as.numeric(rep(vectimes, length.out = n()))) %>%
  dplyr::rename(log2FC = value) %>%
  mutate(type = factor(type, levels = c("Signaling molecules", "Transcription factors", "Others"))) %>%
  filter(!str_detect(time, pattern = "99")) %>% select(-variable)

# Select which sequences are labelled on the figure
selection_AR_FC <- candidates_AR_FC_annotated  %>%
  filter(time == max(time)) %>% mutate(regul = if_else(log2FC > 0, "up", "down")) %>%
  group_by(type, regul) %>%
  top_n(n = 10, wt = abs(log2FC))

ggplot(mapping = aes(x = as.factor(time), y = log2FC, group = names)) +
  geom_line(data = anti_join(candidates_AR_FC_annotated, selection_AR_FC, by = "names"), color = "darkgrey", show.legend = F, size = 0.2, alpha = 0.75) +
  geom_line(data = semi_join(candidates_AR_FC_annotated, selection_AR_FC, by = "names"), aes(color = names), show.legend = F, size = 0.3, alpha = 1) +
  geom_line(data = filter(candidates_AR_FC_annotated, names == "seq68062_loc22828"), color = "red", size = 1) +
  geom_point(data = semi_join(candidates_AR_FC_annotated, selection_AR_FC, by = "names"), aes(color = names), size = 0.3, show.legend = F, alpha = 1) +
  geom_text_repel(data = selection_AR_FC, aes(label = substr(names, 0, 8), group = names, color = names),
                  show.legend = F, size = 2, nudge_x = 100, segment.size = 0.2) +
  expand_limits(x = 14) +
  facet_wrap(~ type) +
  theme_bw() + theme_main + 
  theme(axis.text = element_text(size = 7), axis.title=element_text(size = 9)) +
  xlab("hpa") +
  ylab("Log2FC relative to time = 0") +
  scale_y_continuous(breaks = c(-10:10)) +
  geom_hline(yintercept = c(-1, +1), size = 0.05)
ggsave(filename = paste0(outputdir, "/apical_reg_FC_log_TFs_Signal_", date, ".pdf"), useDingbats = FALSE, width = 16.5, height = 8, units = "cm")


candidates_BR_FC_TF <- candidates_BR_FC %>% gather(variable, value, -names) %>% 
  filter(str_detect(names, str_c(TF_all, collapse = "|"))) %>%
  mutate(type = "Transcription factors")

candidates_BR_FC_SP <- candidates_BR_FC %>% gather(variable, value, -names) %>%
  filter(str_detect(names, str_c(SP_all, collapse = "|"))) %>%
  mutate(type = "Signaling molecules")

candidates_BR_FC_others <- candidates_BR_FC %>% gather(variable, value, -names) %>%
  filter(!str_detect(names, str_c(SP_all, collapse = "|"))) %>%
  filter(!str_detect(names, str_c(TF_all, collapse = "|"))) %>%
  mutate(type = "Others")

candidates_BR_FC_annotated <- rbind(candidates_BR_FC_TF, candidates_BR_FC_SP, candidates_BR_FC_others) %>%
  arrange(names, variable) %>% mutate(time = as.numeric(rep(vectimes, length.out = n()))) %>%
  dplyr::rename(log2FC = value) %>%
  mutate(type = factor(type, levels = c("Signaling molecules", "Transcription factors", "Others"))) %>%
  filter(!str_detect(time, pattern = "99")) %>% select(-variable)

# FC figures
selection_BR_FC <- candidates_BR_FC_annotated %>% filter(!str_detect(time, pattern = "99")) %>%
  filter(time == max(time)) %>% mutate(regul = if_else(log2FC > 0, "up", "down")) %>%
  group_by(type, regul) %>%
  top_n(n = 10, wt = abs(log2FC))

ggplot(mapping = aes(x = as.factor(time), y = log2FC, group = names)) +
  geom_hline(yintercept = c(-1, +1), size = 0.05) +
  geom_line(data = anti_join(candidates_BR_FC_annotated, selection_BR_FC, by = "names"), color = "darkgrey", show.legend = F, size = 0.2, alpha = 0.75) +
  geom_line(data = semi_join(candidates_BR_FC_annotated, selection_BR_FC, by = "names"), aes(color = names), show.legend = F, size = 0.3, alpha = 1) +
  geom_line(data = filter(candidates_BR_FC_annotated, names == "seq68062_loc22828"), color = "red", size = 0.5) +
  geom_point(data = semi_join(candidates_BR_FC_annotated, selection_BR_FC, by = "names"), aes(color = names), size = 0.3, show.legend = F, alpha = 1) +
  geom_text_repel(data = selection_BR_FC, aes(label = substr(names, 0, 8), group = names, color = names),
                  show.legend = F, size = 2, nudge_x = 100, segment.size = 0.2) +
  expand_limits(x = 14) +
  facet_wrap(~ type) +
  theme_bw() + theme_main + 
  theme(axis.text = element_text(size = 7), axis.title=element_text(size = 9)) +
  xlab("hpa") +
  ylab("Log2FC relative to time = 0") +
  scale_y_continuous(breaks = c(-10:10))
ggsave(filename = paste0(outputdir, "/basal_reg_FC_log_TFs_Signal_", date, ".pdf"), useDingbats = FALSE, width = 16.5, height = 8, units = "cm")




# This part to produce gene panels for Supplement corresponding to Fig. 07

setwd("/Users/Yvan/Documents/Publications_PROJECTS/hydratlas_now/")
inputdir <- "/Users/Yvan/Documents/Publications_PROJECTS/hydratlas_now/Rcode/"
date <- Sys.Date()
version <- "hydratlas_v3/"
dir.create(version, showWarnings = F)

dir <- "Figure07"
fig <- "Fig"
sfig <- "SFig"
stable <- "STable"
dir.create(paste0(version, "/", dir), showWarnings = F)
outputdir <- paste0(version, "/", dir)

theme_main <- theme(strip.text = element_text(size = 8), axis.text = element_text(size = 7.5), axis.title = element_text(size = 9))
# library(DESeq2)
# library(ggrepel)
# library(reshape2)
# library(grid)
# library(openxlsx)
# library(cluster)
# library(ggtern)
# library(RColorBrewer)
# library(ImpulseDE2)
# library(pheatmap)

# Although the detection of interesting candidates was made on AR-50 and BR-50 datasets independently
# I would like now perform the nornormalization on the full dataset

countData_reg <- countData[,c(1:99)]
countData_regSlim <- countData_reg[which(apply(countData_reg, 1, max) >= 100),]
rownames(countData_regSlim) <- substr(rownames(countData_regSlim), 0, 17)
colData_reg <- data.frame(condition = c(rep(c("BR50", "AR50", "AR80"), each  = 30), rep(c("Basal", "Apical", "Intact"), each = 3)), hours = c(rep(c(0, 0.5, 1, 2, 4, 8, 16, 24, 36, 48), each  = 3, time = 3), rep(53.5, time = 9)), row.names = colnames(countData_regSlim))

dds_reg <- DESeqDataSetFromMatrix(countData = countData_regSlim, colData = colData_reg, design = ~ condition + hours)
dds_reg <- DESeq(dds_reg, parallel = TRUE, betaPrior = TRUE)

normacounts_reg <- counts(dds_reg, normalized = T) %>%
  as.data.frame() %>%
  rownames_to_column(var = "names") %>%
  melt(id = "names") %>% arrange(names) %>% dplyr::rename("reads" = value)

normacounts_reg$condition <- colData_reg$condition
normacounts_reg$condition <- factor(colData_reg$condition, level = c("Apical", "AR80", "BR50", "AR50", "Basal", "Intact"), labels = c("Apical Homeostatic", "Apical Reg. 80%", "Basal Reg. 50%", "Apical Reg. 50%", "Basal Homeostatic", "Full Homeostatic"))
normacounts_reg$time <- as.numeric(colData_reg$hours)


candidates_apical <- left_join(candidates_AR_FC_annotated %>% as_tibble() %>%
                                 dplyr::select(names, type), normacounts_reg, by = "names")
candidates_basal <- left_join(candidates_BR_FC_annotated %>% as_tibble() %>%
                                dplyr::select(names, type), normacounts_reg, by = "names")

fix.size.and.save.a4 <- function(g, len.w = 2.75, len.h = 2.25, filename) {
    require(grid)
  p <- ggplotGrob(g)
  len.w <- unit(len.w, "cm")
  len.h <- unit(len.h, "cm")
  slots <- grep("panel", p$layout$name)
  lenw <- length(unique(p$layout$l[slots]))
  lenh <- length(unique(p$layout$t[slots]))
  p$widths[unique(p$layout$l[slots])] <-  rep(len.w,  lenw)
  p$heights[unique(p$layout$t[slots])] <-  rep(len.h,  lenh)
  if (!is.null(filename)) {
    pdf(file = filename, width=unit(8.27, "in"), height=unit(11.69, "in"), useDingbats = F)
    grid.draw(p)
    dev.off()
  }
}

p1 <- ggplot(candidates_apical, aes(x = time, y = reads/1000, fill = condition)) +
  geom_smooth(alpha=0.2,span=0.3,size=0.5,aes(color = condition),se = T, method = "loess", show.legend = FALSE) +
  geom_point(alpha=0.6,size = 1.75, shape=21,color="black",stroke=0.35) +
  scale_fill_manual(breaks=c("Apical Reg. 80%", "Apical Reg. 50%", "Basal Reg. 50%", "Apical Homeostatic", "Basal Homeostatic", "Full Homeostatic"),
                    values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  scale_color_manual(values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  theme_bw() +
  guides(fill = guide_legend(override.aes = list(alpha=1))) +  
  theme(axis.title.y = element_text(size = 9, vjust = -1, color = "black"),
        axis.title.x = element_text(size = 9), 
        axis.text.y = element_text(margin = margin(0.5), size = 8, color = "black"), 
        axis.text.x = element_text(margin = margin(0.5), size = 7, color = "black"),
        legend.key.size = unit(0.75,"cm"),
        legend.position = "top",
        strip.text = element_text(size = 9, family="Helvetica", face = "italic")) +
  guides(color = FALSE) +
  facet_wrap(~names ,  scales ="free_y", ncol = 5) +
  ylab("Estimated counts (thousands)") +
  xlab("Hours post amputation") +
  expand_limits(y = 0) +
  scale_x_continuous(breaks = c(0, 2, 4, 8, 16, 24, 36, 48, 53.5), labels = c("0","2","4","8","16","24","36","48","Hom")) +
  theme(panel.spacing.x = unit(.15, "lines"))

#fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "example_data_obtained_regminigraphs_", date, ".pdf"), len.w = 3.75, len.h = 2.5)
fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "candidates_apical_minigraphsALL_", date, ".pdf"), len.w = 3, len.h = 1.5)

candidates_apical_curated_list <- read_tsv(paste0(inputdir, "apical_candidates_curated.csv")) %>% semi_join(candidates_apical, .)

p1 <- ggplot(candidates_apical_curated_list, aes(x = time, y = reads/1000, fill = condition)) +
  geom_smooth(alpha=0.2,span=0.3,size=0.5,aes(color = condition),se = T, method = "loess", show.legend = FALSE) +
  geom_point(alpha=0.6,size = 1.75, shape=21,color="black",stroke=0.35) +
  scale_fill_manual(breaks=c("Apical Reg. 80%", "Apical Reg. 50%", "Basal Reg. 50%", "Apical Homeostatic", "Basal Homeostatic", "Full Homeostatic"),
                    values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  scale_color_manual(values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  theme_bw() +
  guides(fill = guide_legend(override.aes = list(alpha=1))) +  
  theme(axis.title.y = element_text(size = 9, vjust = -1, color = "black"),
        axis.title.x = element_text(size = 9), 
        axis.text.y = element_text(margin = margin(0.5), size = 8, color = "black"), 
        axis.text.x = element_text(margin = margin(0.5), size = 7, color = "black"),
        legend.key.size = unit(0.75,"cm"),
        legend.position = "top",
        strip.text = element_text(size = 9, family="Helvetica", face = "italic")) +
  guides(color = FALSE) +
  facet_wrap(~names ,  scales ="free_y", ncol = 5) +
  ylab("Estimated counts (thousands)") +
  xlab("Hours post amputation") +
  expand_limits(y = 0) +
  scale_x_continuous(breaks = c(0, 2, 4, 8, 16, 24, 36, 48, 53.5), labels = c("0","2","4","8","16","24","36","48","Hom")) +
  theme(panel.spacing.x = unit(.15, "lines"))
fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "candidates_apical_minigraphsCURATED_", date, ".pdf"), len.w = 3, len.h = 1.5)




p1 <- ggplot(candidates_basal, aes(x = time, y = reads/1000, fill = condition)) +
  geom_smooth(alpha=0.2,span=0.3,size=0.5,aes(color = condition),se = T, method = "loess", show.legend = FALSE) +
  geom_point(alpha=0.6,size = 1.75, shape=21,color="black",stroke=0.35) +
  scale_fill_manual(breaks=c("Apical Reg. 80%", "Apical Reg. 50%", "Basal Reg. 50%", "Apical Homeostatic", "Basal Homeostatic", "Full Homeostatic"),
                    values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  scale_color_manual(values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  theme_bw() +
  guides(fill = guide_legend(override.aes = list(alpha=1))) +  
  theme(axis.title.y = element_text(size = 9, vjust = -1, color = "black"),
        axis.title.x = element_text(size = 9), 
        axis.text.y = element_text(margin = margin(0.5), size = 8, color = "black"), 
        axis.text.x = element_text(margin = margin(0.5), size = 7, color = "black"),
        legend.key.size = unit(0.75,"cm"),
        legend.position = "top",
        strip.text = element_text(size = 9, family="Helvetica", face = "italic")) +
  guides(color = FALSE) +
  facet_wrap(~names ,  scales ="free_y", ncol = 5) +
  ylab("Estimated counts (thousands)") +
  xlab("Hours post amputation") +
  expand_limits(y = 0) +
  scale_x_continuous(breaks = c(0, 2, 4, 8, 16, 24, 36, 48, 53.5), labels = c("0","2","4","8","16","24","36","48","Hom")) +
  theme(panel.spacing.x = unit(.15, "lines"))

#fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "example_data_obtained_regminigraphs_", date, ".pdf"), len.w = 3.75, len.h = 2.5)
fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "candidates_basal_minigraphsALL_", date, ".pdf"), len.w = 3, len.h = 1.5)

candidates_basal_curated_list <- read_tsv(paste0(inputdir, "basal_candidates_curated.csv")) %>% semi_join(candidates_basal, .)

p1 <- ggplot(candidates_basal_curated_list, aes(x = time, y = reads/1000, fill = condition)) +
  geom_smooth(alpha=0.2,span=0.3,size=0.5,aes(color = condition),se = T, method = "loess", show.legend = FALSE) +
  geom_point(alpha=0.6,size = 1.75, shape=21,color="black",stroke=0.35) +
  scale_fill_manual(breaks=c("Apical Reg. 80%", "Apical Reg. 50%", "Basal Reg. 50%", "Apical Homeostatic", "Basal Homeostatic", "Full Homeostatic"),
                    values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  scale_color_manual(values=c("Apical Reg. 80%" = "#6699FF", "Apical Reg. 50%" = "#33CCCC", "Basal Reg. 50%" = "#FF9933", "Apical Homeostatic" = "#CC00CC", "Basal Homeostatic" = "#FFCC33", "Full Homeostatic" = "grey")) +
  theme_bw() +
  guides(fill = guide_legend(override.aes = list(alpha=1))) +
  theme(axis.title.y = element_text(size = 9, vjust = -1, color = "black"),
        axis.title.x = element_text(size = 9),
        axis.text.y = element_text(margin = margin(0.5), size = 8, color = "black"),
        axis.text.x = element_text(margin = margin(0.5), size = 7, color = "black"),
        legend.key.size = unit(0.75,"cm"),
        legend.position = "top",
        strip.text = element_text(size = 9, family="Helvetica", face = "italic")) +
  guides(color = FALSE) +
  facet_wrap(~names ,  scales ="free_y", ncol = 5) +
  ylab("Estimated counts (thousands)") +
  xlab("Hours post amputation") +
  expand_limits(y = 0) +
  scale_x_continuous(breaks = c(0, 2, 4, 8, 16, 24, 36, 48, 53.5), labels = c("0","2","4","8","16","24","36","48","Hom")) +
  theme(panel.spacing.x = unit(.15, "lines"))
fix.size.and.save.a4(p1, filename = paste0(outputdir, "/", sfig, "_", "candidates_basal_minigraphsCURATED_", date, ".pdf"), len.w = 3, len.h = 1.5)



